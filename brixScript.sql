USE [master]
GO
/****** Object:  Database [BrixDB]    Script Date: 01/04/2020 22:21:27 ******/
CREATE DATABASE [BrixDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BrixDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\BrixDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BrixDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\BrixDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [BrixDB] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BrixDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BrixDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BrixDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BrixDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BrixDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BrixDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [BrixDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BrixDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BrixDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BrixDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BrixDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BrixDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BrixDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BrixDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BrixDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BrixDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BrixDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BrixDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BrixDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BrixDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BrixDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BrixDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BrixDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BrixDB] SET RECOVERY FULL 
GO
ALTER DATABASE [BrixDB] SET  MULTI_USER 
GO
ALTER DATABASE [BrixDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BrixDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BrixDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BrixDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BrixDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BrixDB', N'ON'
GO
ALTER DATABASE [BrixDB] SET QUERY_STORE = OFF
GO
USE [BrixDB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [BrixDB]
GO
/****** Object:  User [M3New]    Script Date: 01/04/2020 22:21:27 ******/

/****** Object:  User [IIS APPPOOL\BrixServer]    Script Date: 01/04/2020 22:21:27 ******/


/****** Object:  Table [dbo].[failedTBL]    Script Date: 01/04/2020 22:21:27 ******/

CREATE TABLE [dbo].[failedTBL](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[ip] [nvarchar](50) NULL,
	[description] [nvarchar](50) NULL,
 CONSTRAINT [PK_failedTBL] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[whiteListTBL]    Script Date: 01/04/2020 22:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[whiteListTBL](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[ip] [nvarchar](50) NULL,
	[description] [nchar](10) NULL,
 CONSTRAINT [PK_whiteListTBL] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[addFailedIP]    Script Date: 01/04/2020 22:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[addFailedIP]
@ip nvarchar(50)
as
begin
insert into [dbo].[failedTBL]([ip])
values(@ip)
end
GO
/****** Object:  StoredProcedure [dbo].[addWhiteIP]    Script Date: 01/04/2020 22:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[addWhiteIP]
@ip varchar(30), @description varchar(200)
as
begin
insert into [dbo].[whiteListTBL]
values(@ip,@description)
end
GO
/****** Object:  StoredProcedure [dbo].[deleteFailedIP]    Script Date: 01/04/2020 22:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[deleteFailedIP]
@ip nvarchar(50)
as
begin
delete from [dbo].[failedTBL] where
[iP]=@ip
end
GO
/****** Object:  StoredProcedure [dbo].[deleteWhiteIP]    Script Date: 01/04/2020 22:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[deleteWhiteIP]
@ip varchar(30)
as
begin
delete from [dbo].[whiteListTBL]
where [ip]=@ip
end
GO
/****** Object:  StoredProcedure [dbo].[editDescreption]    Script Date: 01/04/2020 22:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[editDescreption]
@ip nvarchar(50),
@description nvarchar(50)
as 
begin
update [dbo].[WhiteListTBL]
set [description]=@description
where [ip]=@ip
end
GO
/****** Object:  StoredProcedure [dbo].[editIP]    Script Date: 01/04/2020 22:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[editIP]
@id smallint,
@ip nvarchar(50),
@description nvarchar(50)
as
begin 
update [dbo].[whiteListTBL]
set [description]=@description, [ip]=@ip
where [id]=@id
end
GO
/****** Object:  StoredProcedure [dbo].[editIpDescreption]    Script Date: 01/04/2020 22:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[editIpDescreption]
@id smallint,
@ip nvarchar(50),
@description nvarchar(50)
as 
begin
update [dbo].[WhiteListTBL]
set [description]=@description,
[ip]=@ip
where [id]=@id
end
GO
/****** Object:  StoredProcedure [dbo].[getWhiteList]    Script Date: 01/04/2020 22:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[getWhiteList]
as
begin 
select * from [dbo].[whiteListTBL]
end
GO
/****** Object:  StoredProcedure [dbo].[getWhiteList1]    Script Date: 01/04/2020 22:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[getWhiteList1]
as
begin 
select * from [dbo].[whiteListTBL]
end
GO
/****** Object:  StoredProcedure [dbo].[isWhiteIP]    Script Date: 01/04/2020 22:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[isWhiteIP]
@ip nvarchar(50)
as begin
if((select IP from [dbo].[WhiteListTBL] where [IP]=@ip)is null)
select 0
else
select 1
end
GO
USE [master]
GO
ALTER DATABASE [BrixDB] SET  READ_WRITE 
GO
